import {Component, NgFor, NgIf, View} from 'angular2/angular2';
import {Http, HTTP_PROVIDERS} from 'angular2/http';
import {ROUTER_DIRECTIVES, RouteParams} from 'angular2/router';
import {QuoteRepository} from "../model/repository/quote";
import {QuoteCollection} from "../model/quoteCollection";
import {QuoteViewComponent} from "./quoteView";

@Component({selector: 'quote-top', providers: [HTTP_PROVIDERS]})
@View({
    directives:  [NgFor, NgIf, ROUTER_DIRECTIVES, QuoteViewComponent],
    templateUrl: 'templates/quote-top.html',
})
export class QuoteTopComponent
{
    quotes = new QuoteCollection([], 0);
    links = [];

    constructor(quoteRepository: QuoteRepository, routeParams: RouteParams) {
        let currentPage = parseInt(routeParams.get('page')) || 1;

        quoteRepository
            .findBestRateds(currentPage)
            .subscribe(
                data => {
                    this.quotes = data;

                    for (let i = 0; i < this.quotes.totalItems / 10 ;i++) {
                        this.links.push({active: (currentPage) === i + 1, page: i+1, text: i+1});
                    }
                },
                err  => console.log(err)
            );
    }
}
