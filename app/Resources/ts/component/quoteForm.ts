import {Component, View, CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/angular2';
import {RouteParams, Router} from 'angular2/router';
import {Quote} from "../model/quote";
import {QuoteRepository} from "../model/repository/quote";
import {QuoteViewComponent} from "./quoteView";

@Component({selector: 'quote-form'})
@View({
    templateUrl: 'templates/quote-form.html',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, QuoteViewComponent]
})
export class QuoteFormComponent
{
    quote: Quote = new Quote({});

    constructor(private quoteRepository: QuoteRepository, private router: Router) {}

    onSubmit(hf) {
        if (!hf.form.valid) {
            return;
        }

        this.quoteRepository
            .persist(this.quote)
            .subscribe(quote => this.router.navigate(['/Quotes_Show', {id: quote.id}]));
    }
}
