import {Component, View} from 'angular2/angular2';
import {RouteParams, ROUTER_DIRECTIVES } from 'angular2/router';
import {Quote} from "../model/quote";
import {QuoteRepository} from "../model/repository/quote";
import {QuoteViewComponent} from "./quoteView";

@Component({selector: 'quote'})
@View({templateUrl: 'templates/quote.html', directives: [QuoteViewComponent]})
export class QuoteComponent
{
    quote: Quote = new Quote({});

    constructor(params: RouteParams, quoteRepository: QuoteRepository) {
        quoteRepository
            .findOneById(params.get('id'))
            .subscribe(quote => this.quote = quote);
    }
}
