import {Component, View, Attribute} from 'angular2/angular2';
import {ROUTER_DIRECTIVES } from 'angular2/router';
import {Quote} from "../model/quote";
import {QuotifyPipe} from "../pipe/quotify";
import {MomentPipe} from "../pipe/moment";
import {QuoteRepository} from "../model/repository/quote";

@Component({selector: 'quote-view', properties: ['quote']})
@View({templateUrl: 'templates/quote-view.html', directives: [ROUTER_DIRECTIVES], pipes: [QuotifyPipe, MomentPipe]})
export class QuoteViewComponent
{
    constructor(@Attribute('quote') public quote: Quote, public quoteRepository: QuoteRepository) {}

    voteUp() {
        this.quoteRepository
            .voteUp(this.quote)
            .subscribe(quote => this.quote = quote);
    }

    voteDown() {
        this.quoteRepository
            .voteDown(this.quote)
            .subscribe(quote => this.quote = quote);
    }
}
