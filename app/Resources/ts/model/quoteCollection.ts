import {Quote} from "./quote";

export class QuoteCollection
{
    public quotes: Quote[];
    public totalItems: number;
    public itemsPerPage: number;

    constructor(hydraData: any, public currentPage: number) {
        if (undefined !== hydraData['hydra:member']) {
            this.quotes = hydraData['hydra:member'].map(member => new Quote(member));
            this.totalItems = hydraData['hydra:totalItems'];
            this.itemsPerPage = hydraData['hydra:itemsPerPage'];
        }
    }

    getQuotes() {
        return this.quotes;
    }
}
