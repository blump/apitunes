export class Quote {
    id:number;
    title:string;
    author:string;
    text:string;
    vote:number;
    publishedAt:Date;
    updatedAt:Date;

    constructor(data:Object) {
        this.publishedAt = new Date;
        this.updatedAt = new Date;
        this.vote = 0;

        if (undefined !== data['@id']) {
            this.id = data['@id'].replace(/\/quotes\/(\d+)/ig, '$1');
            this.title = data['title'];
            this.author = data['author'];
            this.text = data['text'];
            this.publishedAt = new Date(data['publishedAt']);
            this.updatedAt = new Date(data['updatedAt']);
            this.vote = data['vote'];
        }
    }
}
