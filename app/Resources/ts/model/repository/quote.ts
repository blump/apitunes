import { Component, Injectable, Inject } from 'angular2/angular2';
import { Http, HTTP_PROVIDERS, Response } from 'angular2/http';
import { Quote } from '../quote';
import {Configuration} from "../../configuration";
import {QuoteCollection} from "../quoteCollection";

@Injectable()
export class QuoteRepository
{
    constructor(public http: Http, public config: Configuration) { }

    findLasts(page: number = 1): any {
        return this.http.get(this.config.get('api.endpoint') + 'quotes?order[publishedAt]=DESC&page=' + page)
            .map((res: Response) => res.json())
            .map(data => new QuoteCollection(data, page))
    }

    findOneById(id): any {
        return this.http.get(this.config.get('api.endpoint') + 'quotes/' + id)
            .map((res: Response) => res.json())
            .map(data => new Quote(data))
    }

    findBestRateds(page: number = 1): any {
        return this.http.get(this.config.get('api.endpoint') + 'quotes?order[vote]=DESC&page=' + page)
            .map((res: Response) => res.json())
            .map(data => new QuoteCollection(data, page))
    }

    persist(quote: Quote): any {
        return this.http.post(this.config.get('api.endpoint') + 'quotes', JSON.stringify(quote))
            .map((res: Response) => res.json())
            .map(data => new Quote(data))
    }

    voteUp(quote: Quote): any {
        return this.http.put(this.config.get('api.endpoint') + 'quotes/' + quote.id + '/vote_up', '')
            .map((res: Response) => res.json())
            .map(data => new Quote(data))
    }

    voteDown(quote: Quote): any {
        return this.http.put(this.config.get('api.endpoint') + 'quotes/' + quote.id + '/vote_down', '')
            .map((res: Response) => res.json())
            .map(data => new Quote(data))
    }
}
