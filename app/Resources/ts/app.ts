import { bootstrap, Component, View, Input, ElementRef } from 'angular2/angular2';
import { Http, HTTP_PROVIDERS } from 'angular2/http';
import { ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig } from 'angular2/router';
import { HomeComponent } from './component/home';
import { QuoteComponent } from './component/quote';
import { QuoteTopComponent } from './component/quoteTop';
import { QuoteRepository } from './model/repository/quote';
import { Configuration } from './configuration';
import { QuoteFormComponent } from "./component/quoteForm";
import {QuoteViewComponent} from "./component/quoteView";

@Component({
    selector: 'apitunes'
})
@View({
    directives:  ROUTER_DIRECTIVES,
    templateUrl: 'templates/layout.html',
})
@RouteConfig([
    { path: '/', name: 'Homepage', redirectTo: '/quotes' },
    { path: '/quotes', name: 'Quotes_List', component: HomeComponent },
    { path: '/quotes/top', name: 'Quotes_Top', component: QuoteTopComponent },
    { path: '/quotes/new', name: 'Quotes_New', component: QuoteFormComponent },
    { path: '/quotes/:id', name: 'Quotes_Show', component: QuoteComponent },
])
class AppComponent
{
    constructor(public elementRef: ElementRef, config: Configuration) {
        config.set('api.endpoint', elementRef.nativeElement.getAttribute('[apiEndpoint]'));
    }
}

bootstrap(
    AppComponent,
    [
        [
            QuoteRepository,
            HTTP_PROVIDERS,
            ROUTER_PROVIDERS,
            Configuration
        ]
    ]
);
