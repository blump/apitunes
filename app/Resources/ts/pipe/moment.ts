///<reference path="../../../../typings/moment/moment.d.ts" />

import {Pipe} from 'angular2/angular2'

@Pipe({name: 'moment'})
export class MomentPipe
{
    transform(value: Date, args:string[]) {
        if ('fromNow' === args[0]) {
            return moment(value).fromNow();
        }

        return moment(value).format(args[0]);
    }
}
