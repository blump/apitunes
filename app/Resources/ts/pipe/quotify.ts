import {Pipe} from 'angular2/angular2'

@Pipe({name: 'quotify'})
export class QuotifyPipe
{
    getNameClass(name: string) {
        return 'quoted-name-' + (this.basicHash(name) % 10);
    }

    basicHash(text: string) {
        let hash = 0;
        for (let i = 0; i < text.length; i++) {
            hash += text.charCodeAt(i);
        }

        return hash;
    }

    transform(value: string) {
        if (!value) {
            return '';
        }

        return value
            .replace(/</ig, '&lt;')
            .replace(/>/ig, '&gt;')
            .replace(/(&lt;)([^&]+)(&gt;)/img, (arg1, arg2, name: string) => {
                return '<span class="quoted-name ' + this.getNameClass(name) + '">&lt;' + name + '&gt; </span>'
            })
            .replace(/^([^:]+)\s*:\s*/img, (arg1, name: string) => {
                return '<span class="quoted-name ' + this.getNameClass(name) + '">&lt;' + name + '&gt; </span>'
            })
            .replace(/^(.*)$/img, '<div class="quote-line">$1</div>');
    }
}
