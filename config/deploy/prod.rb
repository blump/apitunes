server 'fortunes.un-zero-un.net', user: fetch(:ssh_user), roles: %w{app db web}

set :deploy_to, '/home/fortunes/apitunes'
set :stage, :prod

set :default_env, { 'PATH' => '/opt/php-7.0/bin:$PATH' }
