require 'capistrano/composer'
require 'capistrano/symfony'

# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'apitunes'
set :repo_url,    'git@bitbucket.org:unzeroun/apitunes.git'
set :ssh_user,    'fortunes'
set :branch,      'master'

set :linked_files, %w{app/config/parameters.yml}
set :linked_dirs, %w{app/logs}

set :cache_path, "app/cache"
set :symfony_console_path, "app/console"

set :composer_install_flags, '--no-dev --prefer-dist --no-interaction --optimize-autoloader'
set :composer_roles, :web
set :composer_working_dir, -> { fetch(:release_path) }
set :composer_dump_autoload_flags, '--optimize'

set :keep_releases, 3
set :scm, :git
set :format, :pretty
set :log_level, :info

namespace :deploy do
  after 'deploy:updated', :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      within release_path do

        execute :npm, 'install'
        execute :bower, 'install'
        execute :tsd, 'install'
        execute :gulp
        execute :php, 'app/console', 'assets_version:increment'
        execute :php, 'app/console', 'cache:clear', '--env=prod'

      end
    end
  end
  after 'deploy:published', :clear_cache do
      on roles(:web), in: :groups, limit: 3, wait: 10 do
        within release_path do

          execute :sudo, '/usr/bin/supervisorctl', 'restart', 'php7-fpm'
          execute :sudo, '/etc/init.d/apache2', 'graceful'

        end
      end
    end
end
