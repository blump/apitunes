import gulp   from 'gulp';
import concat from 'gulp-concat';
import ts     from 'gulp-typescript';
import rename from 'gulp-rename';

let tsProject = ts.createProject('tsconfig.json', { typescript: require('typescript') });

gulp.task('typescript', () => {
    var tsResult = gulp.src([
        'typings/moment/moment.d.ts',
        'app/Resources/ts/**/*.ts'
    ])
        .pipe(ts(tsProject));

    return tsResult.js
        .pipe(gulp.dest('web/js'));
});

gulp.task('components', () => {
    return gulp.src('app/Resources/components/**/*.html')
        .pipe(gulp.dest('web/templates'));
});

gulp.task('css', () => {
    return gulp.src('app/Resources/css/**/*.css')
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('web/css'));
});

gulp.task('libs', () => {
    return gulp.src([
            'node_modules/systemjs/dist/system.js',
            'node_modules/angular2/bundles/angular2.dev.js',
            'node_modules/angular2/bundles/http.dev.js',
            'node_modules/angular2/bundles/router.dev.js',
            'bower_components/moment/moment.js',
            'bower_components/moment/locale/fr.js'
        ])
        .pipe(concat('libs.js'))
        .pipe(gulp.dest('web/js'));
});


gulp.task('default', ['typescript', 'libs', 'components', 'css']);
gulp.task('watch', ['default'], () => {
    gulp.watch('app/Resources/ts/**/*.ts', ['typescript']);
    gulp.watch('app/Resources/components/**/*.html', ['components']);
    gulp.watch('app/Resources/css/**/*.css', ['css']);
});

