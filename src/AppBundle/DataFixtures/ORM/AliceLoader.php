<?php

namespace AppBundle\DataFixtures\ORM;

use Hautelook\AliceBundle\Alice\DataFixtureLoader;
use Symfony\Component\Finder\Finder;

/**
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class AliceLoader extends DataFixtureLoader
{
    /**
     * {@inheritDoc}
     */
    protected function getFixtures()
    {
        return iterator_to_array(
            Finder::create()
                ->in(__DIR__ . '/../../Resources/fixtures')
                ->name('*.yml')
                ->sortByName()
                ->getIterator()
        );
    }
}
