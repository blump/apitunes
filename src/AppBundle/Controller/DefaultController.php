<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Extra\Route("/")
 *
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class DefaultController extends Controller
{
    /**
     * @Extra\Route("/", name="homepage")
     * @Extra\Route("{path}", name="catch_all", requirements={"path": ".*"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }
}
