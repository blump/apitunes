<?php

namespace AppBundle\Controller;

use Dunglas\ApiBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class QuoteController extends ResourceController
{
    /**
     * @param Request $request
     * @param int     $id
     *
     * @return \Dunglas\ApiBundle\JsonLd\Response
     */
    public function voteUpAction(Request $request, $id)
    {
        $resource = $this->getResource($request);
        $quote    = $this->findOrThrowNotFound($resource, $id);

        $quote->voteUp();
        $this->getDoctrine()->getManager()->flush();

        return $this->getSuccessResponse($resource, $quote);
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @return \Dunglas\ApiBundle\JsonLd\Response
     */
    public function voteDOwnAction(Request $request, $id)
    {
        $resource = $this->getResource($request);
        $quote    = $this->findOrThrowNotFound($resource, $id);

        $quote->voteDown();
        $this->getDoctrine()->getManager()->flush();

        return $this->getSuccessResponse($resource, $quote);
    }
}
